#!/bin/sh
set -e

echo "Building for $1" 
echo "$PATH"

platform="$1"
target=""
case "$platform" in
	"linux/arm/v5")
		target="arm-linux-musleabi"
		;;
	"linux/arm64"*)
		target="aarch64-linux-musl"
		;;
	"linux/amd64"*)
		target="x86_64-linux-musl"
		;;
	"linux/mips"*)
		target="mips-linux-musl"
		;;
	"linux/riscv64"*)
		target="riscv64-linux-musl"
		;;
	*)
		echo "Unknown machine type: $platform"
		exit 1
esac

v -w -v -prealloc -autofree -obf -o vpvwg.c .
# v -prod -w -prealloc -autofree -arch arm -os linux -v .
zig cc -target $target -Oz vpvwg.c -o vpvwg --verbose
