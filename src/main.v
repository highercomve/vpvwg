import time
import os
import strconv

// Importing necessary C functions and constants
fn C.ioctl(fd int, request u32, arg voidptr) int

const (
    watchdog_device = '/dev/watchdog'
    wdioc_gettimeout = u32(0x80045707) // IOCTL request code to get the timeout
    wdioc_keepalive  = u32(0x80045705) // IOCTL request code to ping the watchdog
)

fn read_meta(t string, key string) !string {
    path := "/pantavisor/"+t+"-meta/pvwg."+key
    if !os.exists(path) {
        return error("meta doesn't exists"+key)
    }

    content := os.read_file(path) or {
        return error("can't open the meta file"+key)
    }

	return string(content)
}

fn read_user_meta(key string) !string {
    return read_meta("user", key)
}

// Function to get the watchdog timeout
fn get_watchdog_timeout(fd int) int {
    mut timeout := int(0)
    res := C.ioctl(fd, wdioc_gettimeout, &timeout)
    if res < 0 {
        return 10
    }
    return timeout
}

// Function to ping the watchdog
fn ping_watchdog(fd int) ! {
    res := C.ioctl(fd, wdioc_keepalive, 0)
    if res < 0 {
        return error('Failed to ping watchdog')
    }
}

fn is_disabled() bool {
    disabled := read_user_meta("disabled") or {
        return false
    }
    return disabled == "true"
}

fn get_ping_interval(d int) int {
    pi := read_user_meta("ping-interval") or {
        return d
    }
    ping_interval := strconv.atoi(pi) or {
        return d
    }

    return ping_interval
}

// Main function
fn main() {
    // Open the watchdog device
    mut file := os.open_file(watchdog_device, 'r+') or {
        eprintln('Failed to open watchdog device: $err')
        return
    }
    defer {
        file.close()
    }

    // Get the watchdog timeout
    timeout := get_watchdog_timeout(file.fd)
    mut ping_interval := get_ping_interval(timeout / 2)
    mut disabled := is_disabled()
    is_debug := os.getenv("DEBUG") != ""
    mut print_message := true

    println('Watchdog pinger started. Timeout is $timeout seconds. Pinging every $ping_interval seconds.')
    println('debug mode is set to $is_debug')

    // Pinging loop
    for {
        ping_interval = get_ping_interval(timeout / 2)
        disabled = is_disabled()
        
        if disabled {
            println("Watchdog pinger disabled.")
            continue
        }

        // Ping the watchdog
        ping_watchdog(file.fd) or {
            eprintln(err)
            return
        }
        if print_message {
            println('Watchdog pinged at ' + time.now().utc_string() + '.')
        }
        if !is_debug && print_message {
            print_message = false
        }
        time.sleep(ping_interval * time.second)
    }
}
