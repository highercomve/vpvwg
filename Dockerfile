FROM --platform=$BUILDPLATFORM thevlang/vlang:alpine as zigv

ARG TARGETPLATFORM
ARG BUILDPLATFORM

RUN apk update && apk add --no-cache curl git tar xz

WORKDIR /zig
RUN curl -L https://ziglang.org/download/0.12.0/zig-linux-x86_64-0.12.0.tar.xz | tar xJf - -C ./ && \
  mv zig-linux-x86_64-0.12.0/zig /usr/bin && \
  mv zig-linux-x86_64-0.12.0/lib/* /usr/lib && \
  rm -rf zig-linux-x86_64-0.12.0

FROM --platform=$BUILDPLATFORM zigv as builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

WORKDIR /app
COPY . .

RUN ./build.sh $TARGETPLATFORM

FROM scratch

COPY --from=builder /app/vpvwg /usr/bin/vpvwg

ENTRYPOINT ["/usr/bin/vpvwg"]
